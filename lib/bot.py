#sleekxmpp imports
from sleekxmpp import ClientXMPP
import os

from time import sleep
from log import Log
import subprocess

class Bot(ClientXMPP):    
    def __init__(self, config, commands):
        self.config = config
        self.commands = commands
        ClientXMPP.__init__(self, self.config['jid'], self.config['password'])
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.message)
        self.admin = self.config['admin'].split(', ')
        path = os.getenv("HOME")+'/.giveme/'
        self.log = Log(path)
            
    def session_start(self, event):
        self.send_presence()
        self.get_roster()
        print "Connected"
        for admin in self.admin:
            msg = "Hi " + admin + " I'm online."
            self.send_message(mto=admin, mbody=msg)
        self.log.writeLog("Started")
            
    def message(self, msg):
        admin = str(msg['from']).split('/')[0]
        text = str(msg['body']).strip()
        if admin in self.admin:
            log = admin + ' asked for: ' + text
            print log
            self.log.writeLog(log)
            if text == 'close' or text == 'leave' or text == 'exit':
                out = 'Goodbye ' + admin + '!'
                msg.reply(out).send()
                self.log.writeLog("Closed")
                self.log.closeLogs()
                sleep(2) #without this, bot closes before sendig msg
                exit()
                
            elif text in self.commands:
                command = self.commands[text]
                out = subprocess.check_output(command.split())
                msg.reply(out).send()
                print out
        else:
            auth = "AUTH: " + admin + " without rights asked for " + text
            self.log.writeAuth(auth)
            print auth


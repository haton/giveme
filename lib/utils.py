import os, ConfigParser
from optparse import OptionParser

class Utils:
    def getCommands(self):
        """reads the commands from the config file"""
        optp = OptionParser()
        optp.add_option('-c', '--config', dest='config', help='set configuration file location')
        opts, args = optp.parse_args()
        if opts.config is None:
            opts.config = os.getenv("HOME")+'/.giveme/config'

        #config    
        config = ConfigParser.RawConfigParser()
        config.read(opts.config)
        self.dicConfig = self.configSectionMap('Default', config)

        #commands
        self.dicCommands = self.configSectionMap('Commands', config)
        
    def configSectionMap(self, section, config):
        dic = {}
        for e in config.items(section):
            dic[e[0]] = e[1]
        return dic

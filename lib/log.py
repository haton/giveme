#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import gmtime, strftime
 
class Log:
    """Class to log general info, error info, and injection info"""
    def __init__(self, path):
        self.path = path
        self.openLogs()

    def openLogs(self):
        self.log = open(self.path + 'log.log', 'a')
        self.ilog = open(self.path + 'auth.log', 'a')
        self.elog = open(self.path + 'error.log', 'a')
        
    def writeLog(self, string):
        time = strftime("%d-%m %H:%M:%S", gmtime()) + ": "
        self.log.write(time + string + '\n')

    def writeError(self, string):
        time = strftime("%d-%m %H:%M:%S", gmtime()) + ": "
        self.elog.write(time + string + '\n')

    def writeAuth(self, string):
        """This handles auth, injection, and security things"""
        time = strftime("%d-%m %H:%M:%S", gmtime()) + ": "
        self.ilog.writelines(time + string+'\n')
        
    def closeLogs(self):
        self.log.close()
        self.ilog.close()
        self.elog.close()

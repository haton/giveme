import sys
sys.path.append('lib/')
from utils import Utils
from bot import Bot

def __init__():
    utils = Utils()
    utils.getCommands()
    bot = Bot(utils.dicConfig, utils.dicCommands)
    print "Connecting"
    bot.connect()
    bot.process(block=True)
    print "Closed"
    
__init__()

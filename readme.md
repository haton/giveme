# GiveMe
## Introduction
GiveMe pretends to be a bot that gives you the output from a command. The original idea was taken from [there](http://www.instructables.com/id/Raspberry-remote-control-with-Telegram/).

It's written in Python, using SleekXMPP.

## Config
Install SleekXMPP:

pip install --user sleekxmpp

And copy the config file of the project to `~/.giveme/`.

## Commands
The basic idea is that in the config file you put your commands with these syntaxis:

    keyword = command

Like this:

    [Commands]
    emacs = emacs 
    ip = wget -O - -q icanhazip.com
    chkconfig = chkconfig
    mysql = chkconfig mysql

